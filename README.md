Tiny-tea-temp-timer
===

This project was born out of a love-hate relationship with tea. I love tea, but hate burning my mouth on it when it's too hot and then forgetting about it untill it's too cold. I love it when it's at _just_ the right temperature, but I often miss the window for drinking it there.

This gizmo tracks the temperature of the tea, and alerts you when it has dropped below a certain temperature, so you can start drinking. It will even warn you when the tea drops even further, to warn you that it's leaving the perfect window, and you should drink it as soon as possible.

The prototype looked like this:  
![](./prototype.jpg)

So let me describe what you are looking at.

There is:
- A small prototyping bread board
- A Adafruit Trinket M0 with presoldered headers
- A waterproof DS18B20 digital temperature sensor (this came with the relevant resistor)
- A buzzer
- A lithium Ion Polymer battery - 3.7v 150mAh 
- A JST-PH 2-Pin SMT Breakout Board 
- And, not pictured, a (cute) Adafruit USB Li-Ion/LiPoly charger

After a period of testing in this configuration, I decided it was ready and threw it into it's final form, a part of a old shower gel bottle:
![](./profile-right.jpg)
![](./profile-left.jpg)

This is what it looks like in a cup:

![](./final.jpg)


