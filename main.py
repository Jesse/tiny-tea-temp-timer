# TODO: use the RGB light to give a temp indication
# TODO: research auto shutdown
# TODO: if above succeeds: research pushbutton activation

# Research:
# 80 Too hot
# 70 Too hot
# 65 Starting to get drinkable
# 60 Nice hot
# 55 comfortable
# 50 als je dorst hebt
# 45 lauw
# 40 niet lekker meer

# for trinket support
import board

# voor de termometer
import busio
from adafruit_onewire.bus import OneWireBus
from adafruit_ds18x20 import DS18X20
import time

# for the buzzer
import pulseio
import digitalio

# RGB led
import adafruit_dotstar as dotstar

# Find the termometer and make it addressable
ow = OneWireBus(board.D1)
ds18b20 = DS18X20(ow, ow.scan()[0])

# Create piezo buzzer PWM output.
buzzer = pulseio.PWMOut(board.D0, variable_frequency=True)

# Voor de RGB led
dot = dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1, brightness=0.1)

# ugly hack because I don't know how to do a "first run" of something
def exists(var):
    return var in globals()

###############
# config vars #
###############

# Beep will remind you to shut device off, to save battery
shutDownReminderTime = 60

# Below what temp does the device alert?
notifTemps = [60, 55, 50]

# Testing mode, if set to True this generates fake temp data
testingMode = False

##############
# End config #
##############

# Get temp
def temp():
    if testingMode:
        # Testing temperature is generated here,
        startTemp = 19
        increase = 12
        decrease = 2
        if not exists("C"):
            print("init C")
            global C
            C = 0
#            global peaked
#            peaked = True
            C = startTemp
        if not exists("peaked"):
            if C == 0:
                C = startTemp
                print(C)
                color(C)
                return C
            elif C < 90:
                C += increase
                print(C)
                if C > 80:
                    global peaked
                    peaked = True
                    print ("peaked")
                color(C)
                return C
        else:
            C -= decrease
            print(C)
            time.sleep(0.5)
            color(C)
            return C
        # end temp testing code
    else:
        # This generates actual temperatures via the thermometer
        C = ds18b20.temperature
        print(C)
        color(C)
        return C

# Sends colordata to the RGB chip
def color(localvarTemp):
    dot[0] = wheel(localvarTemp)

# converts 0-100
def wheel(i):
    # if below freezing (unlikely)
    if i < 1:
        return (0, 0, 255)
    # if above 100° (unlikely)
    elif i > 100:
        return (255, 0, 0)
    # if temp is above 50° show green (50) to orange (75) to red (100)
    elif i > 50:
        colorOut = 510-(((i-50) * 510)/50)
        if colorOut < 255:
            return (255, int(colorOut), 0)
        else:
            return (255-int(colorOut-255), 255, 0)
    # If temp is below 50° show green (49) to teal (25) to blue (0)
    elif i < 50:
        colorOut = 510-(((i) * 510)/50)
        if colorOut < 255:
            return (0, 255, int(colorOut))
        else:
            return (0, 255-int(colorOut-255), 255)

# Function that will make the beeper beep
def beeper(beep):
    dot.brightness=0.5 # "Beep" led
    print("beep type chosen:", beep)
    for i in beeperType[beep][0]:
        buzzer.duty_cycle = 2**15 # Turn buzzer on
        buzzer.frequency = i
        time.sleep(beeperType[beep][1][0])
        buzzer.duty_cycle = 0 # turn buzzer off
    # create some time for the increase in brightness to register
    time.sleep(0.5)
    dot.brightness=0.1

# beeperType lists, basically ringtones. Each tone is one line.
# On each line there are two lists, first the notes, then the time.
# Currently for the time we only look at the first value.
beeperType = [
    # 0 waiting (for tea to be poured, or to be shut down)
    [[400, 500, 200], [0.5]],
    # 1 almost done
    [[300, 200], [0.1]],
    # 2 perfect
    [[600, 800, 1000, 100000, 600, 800, 1000], [0.1]],
    # 3 Hurry!
    [[600, 800, 600, 800, 600, 800, 600, 800, 600, 800, 600, 800], [0.05]],
    # 4 Boot up sound
    [[400, 600, 800], [0.1]]
]

# This helps us track the starting state of the temperature
firstRun = True

# Start-up beep, sounds on boot, to let the user know the device is ready
beeper(4)

# This should run on startup, once we know there is a beverage
# we move on to the main portion of the loop
while firstRun == True:
    # Save the temperature to localTemp for use only in this loop
    # to prevent temp() value from changing halfway through loop
    localTemp = temp()
    if localTemp > notifTemps[0]:
        # We turned on the device inside a hot drink
        firstRun = "Hot"
        print("1==1", 1 == 1)
    if localTemp < 40:
        # Device turned on inside a empty cup (or cold drink)
        firstRun = "Cold"
        # waitTime let's us know how long we are waiting for a
        # increased temperature, every 5 min the device will beep
        # so you know to either turn it off, or pour in your
        # hot drink already
        waitTime = 0
        while firstRun == "Cold":
            # since this is it's own loop, this needs to get fresh temp()
            # values, and does not use localTemp
            if temp() > notifTemps[0]:
                firstRun = "Drink poured"
                print("Drink poured!")
            time.sleep(1)
            waitTime += 1
            print("waiting on insertion. WaitTime:", waitTime)
            # The sleep is 1 second, but the code (and taking of temp)
            # also takes time, so the wait time value is roughly in
            # 1.5 seconds
            if waitTime > shutDownReminderTime:
                beeper(0)
                waitTime = 0
    # This section covers a starting temp of between 40 and 60°
    # it may be useless but I will leave it as a stub for now.
    else:
        firstRun = "real close"

# We now know that the thermometer is in a hot drink
# on to the main loop!

# first some constants used by the main loop
notifNumber = 0

# Main loop nice and small!:
while firstRun != "cooled":
    if temp() < notifTemps[notifNumber]:
        notifNumber += 1
        beeper(notifNumber)
        if len(notifTemps) == notifNumber:
            firstRun = "cooled"

# The beeps have beeped. User should turn the device off
waitTime = 0
while firstRun == "cooled":
    print ("waiting to be turned off")
    temp()
    time.sleep(1)
    waitTime += 1
    print("waitTime:", waitTime)
    if waitTime > shutDownReminderTime:
        beeper(0)
        waitTime = 0